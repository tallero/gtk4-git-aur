# Maintainer: Syboxez Blank <@Syboxez:matrix.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>

_pkg="gtk"
_pkgname="${_pkg}4"
_guic="${_pkg}-update-icon-cache"
_guic4="${_pkgname}-update-icon-cache"
pkgbase="${_pkgname}-git"
pkgname=(
  "${pkgbase}"
  "${_pkgname}-demos-git"
  "${_pkgname}-docs-git"
  "${_guic}-git")
pkgver=4.11.4.r319.gb61991a023
pkgrel=1
epoch=1
pkgdesc="GObject-based multi-platform GUI toolkit"
url="https://www.${_pkg}.org/"
arch=(x86_64)
license=(LGPL)
depends=(
  adwaita-icon-theme
  at-spi2-atk
  atk
  cairo
  cantarell-fonts
  dconf
  desktop-file-utils
  fontconfig
  fribidi
  gdk-pixbuf2
  glib2
  graphene
  gst-plugins-bad-libs
  harfbuzz
  iso-codes
  libcloudproviders
  libcolord
  libcups
  libegl
  libepoxy
  libgl
  libjpeg
  libpng
  librsvg
  libtiff
  libx11
  libxcursor
  libxdamage
  libxext
  libxfixes
  libxi
  libxinerama
  libxkbcommon
  libxrandr
  libxrender
  pango
  shared-mime-info
  tracker3
  wayland
)
makedepends=(
  docbook-xsl
  gi-docgen
  git
  gobject-introspection
  meson
  python-docutils
  python-gobject
  sassc
  shaderc
  wayland-protocols
)
checkdepends=(weston)
source=(
  "git+https://gitlab.gnome.org/GNOME/${_pkg}.git"
  # "git+ssh://git@ssh.gitlab.gnome.org/GNOME/${_pkg}"
  "${_guic}".{hook,script}
  "${_pkgname}-querymodules."{hook,script}
)
sha512sums=(
  'SKIP'
  '6b7d8ecf0e98dcb35fac0c5dc96feb4d66c060c4a19801eac385827a601ae430223b3731d0de0845b0cae50e8169a95b1c13357281ca5a7c2c1ca1930163cad9'
  '05929a7816b0fb45918850d172a375a2e6915a75db879fcc2fbe3b58864e76cc50a7767645a545f48b0d15417f30d1b205a77cfd6396a66df49b28f846bf5bbb'
  'a11c04067970f543a904e7ec9d90114ff98960cee8b04ebb6d90360ca7a3c8989eb8eb48954f6093d7a1f852cad0101581c8ca01d713134cb067dad7fcbffa7e'
  '58475589fa1aa7768b1b3118e1d41153b45bcf03c94015250e29328d29b1b145b1ac8c6a58562a09d216ec1fb724329bb2cceaf5ac95e02b3d49156da248bafa')

pkgver() {
  cd "${_pkg}"

  printf "%s.r%s.g%s" "$(grep -m1 'version' meson.build | sed -r 's/([^0-9]*([0-9.]+)).*/\2/')" \
                      "$(git describe --tags --long | cut -d '-' -f 2)" \
                      "$(git rev-parse --short HEAD)"
}

prepare() {
 cd "${_pkg}"
}

build() {
  local meson_options=(
    -D broadway-backend=true
    -D cloudproviders=enabled
    -D colord=enabled
    -D gtk_doc=true
    -D man-pages=true
    -D tracker=enabled
  )

  CFLAGS+=" -DG_DISABLE_CAST_CHECKS"
  arch-meson "${_pkg}" build "${meson_options[@]}"
  meson compile -C build
}

check() (
  export XDG_RUNTIME_DIR="$PWD/runtime-dir" WAYLAND_DISPLAY=wayland-5

  mkdir -p -m 700 "$XDG_RUNTIME_DIR"
  weston --backend=headless-backend.so --socket=$WAYLAND_DISPLAY --idle-time=0 &
  _w=$!

  trap "kill $_w; wait" EXIT

  meson test -C build --print-errorlogs
)

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_gtk4-git() {
  depends+=("${_guic}-git")
  optdepends=('evince: Default print preview command')
  provides=("${_pkgname}=${pkgver}"
            libgtk-4.so)
  conflicts=("${_pkgname}")

  meson install -C build --destdir "${pkgdir}"

  install -Dm644 /dev/stdin "${pkgdir}/usr/share/${_pkg}-4.0/settings.ini" <<END
[Settings]
gtk-icon-theme-name = Adwaita
gtk-theme-name = Adwaita
gtk-font-name = Cantarell 11
END

  install -Dt "${pkgdir}/usr/share/libalpm/hooks" -m644 "${_pkgname}-querymodules.hook"
  install -D "${_pkgname}-querymodules.script" \
             "${pkgdir}/usr/share/libalpm/scripts/${pkgname}-querymodules"

  cd "${pkgdir}"

  _pick demo "usr/bin/${_pkgname}-"{demo,demo-application,icon-browser,node-editor,print-editor,widget-factory}
  _pick demo "usr/share/applications/org.${_pkg}."{Demo4,IconBrowser4,PrintEditor4,WidgetFactory4,"${_pkgname}.NodeEditor"}.desktop
  _pick demo "usr/share/glib-2.0/schemas/org.${_pkg}.Demo4.gschema.xml"
  _pick demo "usr/share/icons/hicolor/"*"/apps/org.${_pkg}."{Demo4,IconBrowser4,PrintEditor4,WidgetFactory4,"${_pkgname}.NodeEditor"}[-.]*
  _pick demo "usr/share/man/man1/${_pkgname}-"{demo,demo-application,icon-browser,node-editor,widget-factory}.1
  _pick demo "usr/share/metainfo/org.${_pkg}."{Demo4,IconBrowser4,PrintEditor4,WidgetFactory4,"${_pkgname}.NodeEditor"}.appdata.xml

  _pick docs usr/share/doc

  # Built by GTK 4, shared with GTK 3
  _pick guic "usr/bin/${_guic4}"
  _pick guic "usr/share/man/man1/${_guic4}.1"


}

package_gtk4-demos-git() {
  pkgdesc+=" (demo applications)"
  depends=("${_pkgname}")
  provides=("${_pkgname}-demos=${pkgver}")
  mv demo/* "$pkgdir"
}

package_gtk4-docs-git() {
  pkgdesc+=" (documentation)"
  provides=("${_pkgname}-docs=${pkgver}")
  depends=()
  mv docs/* "${pkgdir}"
}

package_gtk-update-icon-cache-git() {
  pkgdesc="GTK icon cache updater"
  depends=(gdk-pixbuf2 librsvg hicolor-icon-theme)
  conflicts=("${_guic4}")
  provides+=("${_guic4}=${pkgver}")

  mv guic/* "$pkgdir"
  ln -s "${_guic4}" \
        "${pkgdir}/usr/bin/${_guic}"
  ln -s "${_guic4}.1" \
        "${pkgdir}/usr/share/man/man1/${_guic}.1"

  install -Dt "${pkgdir}/usr/share/libalpm/hooks" \
          -m644 "${_guic}.hook"
  install -D "${_guic}.script" \
             "$pkgdir/usr/share/libalpm/scripts/${_guic}"
}

# vim:set sw=2 sts=-1 et:
